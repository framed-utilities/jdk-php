# JDK-PHP

This is a work-in-progress (WIP) implementation of the OpenJDK/Java Development Kit in PHP.

## Why did you make this?

It was going to be used as part of another project but I decided to move it to a separate repository where
it belongs and set it aside for the time being so I can move on with my existing project in a simpler
PHP-specific path. I plan to pick up development in the future but feel free to have a look and
contribute if you wish.
